package com.testm.fi8.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.testm.fi8.constant.ConsoleMessages;
import com.testm.fi8.enums.PotionType;
import com.testm.fi8.util.RandomEngine;

public abstract class Mob {
    private int attackDamage;
    private int armorClass;

    protected int health;
    protected int goldAmount;
    protected String name;

    protected RandomEngine randomEngine;
    protected Map<PotionType, Integer> potions = new HashMap<PotionType, Integer>();
    protected ArrayList<Item> items = new ArrayList<Item>();


    public Mob(int health, int attackDamage, int goldAmount, int armorClass, String name, int healthPotions, int strengthPotions, RandomEngine randomEngine) {
        this.health = health;
        this.attackDamage = attackDamage;
        this.goldAmount = goldAmount;
        this.armorClass = armorClass;
        this.name = name;
        this.randomEngine = randomEngine;

        potions.put(PotionType.HEALTH, 5);
        potions.put(PotionType.STRENGTH, 1);        
    }

    public Mob(int health, int attackDamage, int goldAmount, int armorClass, String name, RandomEngine randomEngine) {
        this(health, attackDamage,goldAmount,armorClass,name,0,0,randomEngine);
    }

    public boolean isAlive() {
        return health > 0;
    }

    public void performHit(Mob victim) {
        if(health <= 0) {
            return; // The dead can't fight!
        }

        int damageDealt = getAttackDamage() - victim.armorClass;
        if(damageDealt < 0) {
            damageDealt = 0;
        }

        victim.health -= damageDealt;

        sendTextToMob(ConsoleMessages.STRIKE_DAMAGE_NPC + victim.name + ". " + ConsoleMessages.DAMAGE + damageDealt);
        victim.sendTextToMob(name + ConsoleMessages.STRIKE_DAMAGE_PLAYER + ". " + ConsoleMessages.DAMAGE + damageDealt);       
    }

    protected void equipItem(Item item) {
        items.add(item);
        attackDamage += item.getAttackDamage();
        armorClass += item.getAcAdjust();
        if(item.getAttackMultiplier() > 0) {
            attackDamage *= item.getAttackMultiplier();
        }
    }

    public void addToInventory(PotionType potionType, int count) {
        potions.replace(potionType, potions.get(potionType) + count);       
    }

    public void addToInventory(PotionType potionType) {
        potions.replace(potionType, potions.get(potionType) + 1);       
    }

    public int getNumberOfPotions(PotionType potionType) {
        return potions.get(potionType);
    }

    public int getAttackDamage() {
        return randomEngine.nextInt(1, attackDamage);
    }

    public abstract void sendTextToMob(String text);
    public abstract String getInputFromMob();

	public int getArmorClass() {
		return armorClass;
	}

	public int getHealth() {
		return health;
	}

	public int getGoldAmount() {
		return goldAmount;
	}

	public String getName() {
		return name;
	}

	public RandomEngine getRandomEngine() {
		return randomEngine;
	}

	public Map<PotionType, Integer> getPotions() {
		return potions;
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	public void setHealth(int health) {
		this.health = health;
	}
}
