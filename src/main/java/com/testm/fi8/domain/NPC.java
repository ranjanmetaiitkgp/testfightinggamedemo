package com.testm.fi8.domain;

import com.testm.fi8.enums.PotionType;
import com.testm.fi8.util.RandomEngine;

public class NPC extends Mob {
    public NPC(String name, int health, int attackDamage, int potionDropChance, int goldDropChance, int goldAmount, int armorClass, int specialAttackThreshold, int specialAttack, int specialDropChance, RandomEngine randomEngine) {
        super(health, attackDamage, 0, armorClass, name, randomEngine);


        if(randomEngine.nextInt(0, 100) < goldDropChance)          
        {
            this.goldAmount = goldAmount;
        }

        if(randomEngine.nextInt(0,100) < potionDropChance)       
        {
            addToInventory(PotionType.HEALTH);
            addToInventory(PotionType.STRENGTH);
        }

        if (randomEngine.nextInt(0,100) < specialDropChance)
        {           
            equipItem(new Item("ruby sword of power", 10000, 0, 0, 5));
        }


        this.specialAttack = specialAttack;
        this.specialAttackThreshold = specialAttackThreshold;
    }

    public void performHit(Mob victim) {
        super.performHit(victim);

        if(specialAttackThreshold > 0 && health < specialAttackThreshold && health > 0) {
            int specialAttackDamage = randomEngine.nextInt(10, specialAttack);

            victim.sendTextToMob("\n!!!# The " + name + " unleashes its special attack #!!!");

            health -= specialAttackDamage;

            victim.sendTextToMob(">!!!# You recieve " + specialAttackDamage + " in retaliation from the " + name + "'s fiery breath! #!!!");            
        }
    }


    @Override
    public void sendTextToMob(String text) {
        // NOP
    }

    @Override
    public String getInputFromMob() {
        return "";  // Mobs never want to do anything! 
    }

    private int specialAttack;
    private int specialAttackThreshold;
}