package com.testm.fi8.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.testm.fi8.enums.EquipmentType;
import com.testm.fi8.enums.ItemType;
import com.testm.fi8.enums.PotionType;
import com.testm.fi8.util.RandomEngine;

public class Shop {
    public Shop(RandomEngine randomEngine) {
        this.randomEngine = randomEngine;
        populateShop();     
    }

    public Item getRandomItemForSale() {
        Item itemForSale = shopItems.get(randomEngine.nextInt(0, shopItems.size()));
        return itemForSale;
    }

    public Item getPotion(PotionType potionType) {
        return potions.get(potionType);
    }

    public void purchase(Item itemToPurchase) {
        shopItems.remove(itemToPurchase);
    }

    public void purchasePotion(PotionType potionType, int numberToPurchase) {
        // NOP?!?
    }

   private void populateShop() {
        shopItems.add(new Item(EquipmentType.SILVER_SWORD.getEquipmentName(), 1000, 100, 0, 0));
        shopItems.add(new Item(EquipmentType.STEEL_SWORD.getEquipmentName(), 250, 250, 0, 0));
        shopItems.add(new Item(EquipmentType.IRON_HELMET.getEquipmentName(), 150,0,10, 0));
        shopItems.add(new Item(EquipmentType.IRON_CHESTPLATE.getEquipmentName(), 200, 0, 18, 0));
        shopItems.add(new Item(EquipmentType.IRON_BOOTS.getEquipmentName(), 100, 0, 8, 0));
        shopItems.add(new Item(EquipmentType.IRON_GAUNTLETS.getEquipmentName(), 75, 0, 5, 0));
        shopItems.add(new Item(EquipmentType.STEEL_HELMET.getEquipmentName(), 400, 0, 5, 0));
        shopItems.add(new Item(EquipmentType.STEEL_CHESTPLATE.getEquipmentName(), 600, 0, 10, 0));
        shopItems.add(new Item(EquipmentType.STEEL_BOOTS.getEquipmentName(), 300, 0, 10, 0));
        shopItems.add(new Item(EquipmentType.STEEL_GAUNTLETS.getEquipmentName(), 250, 0, 7, 0));
        shopItems.add(new Item("Illbane", 2500, ItemType.ILLBANE));

        potions.put(PotionType.STRENGTH, new Item("Strength Potion", 500, PotionType.STRENGTH));
        potions.put(PotionType.HEALTH, new Item("Health Potion", 100, PotionType.HEALTH));
    }

    private List<Item> shopItems = new ArrayList<Item>();
    private Map<PotionType, Item> potions = new HashMap<PotionType, Item>();
    private RandomEngine randomEngine;

}