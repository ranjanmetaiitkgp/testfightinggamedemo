package com.testm.fi8.domain;

import java.util.Map;
import java.util.Scanner;

import com.testm.fi8.enums.PotionType;
import com.testm.fi8.util.RandomEngine;

public class Player extends Mob {
	
	private Scanner scanner;
    private int illBaneCount;
    private int strengthEffect;
    private boolean illBaneAct;
	
    public Player(String name, RandomEngine randomEngine, Scanner scanner) {
        super (200, 50, 1000, 0, name, randomEngine);
        illBaneCount = 4;
        strengthEffect = 1;
        this.scanner = scanner;
    }

    public void purchasedItem(Item item) {
        goldAmount-= item.getValue();
        if(item.isEquipment()) {
            equipItem(item);
        } else {
            addToInventory(item, 1);
        }       
    }

    public boolean canAfford(int amount) {
        return goldAmount >= amount;
    }

    public void purchasedPotions(Item potion, int count) {
        goldAmount-= potion.getValue() * count;

        addToInventory(potion, count);  
    }

    public void drinkPotion(PotionType potionType) {
        final int healthPotionEffect = 30;                                    // How much each potion will heal.
        int potionCount = potions.get(potionType);
        String potionName = potionType == PotionType.HEALTH ? "health" : "strength";

        if(potionCount <= 0) {
            sendTextToMob("> You have no " + potionName + " potions left! ");
            return;
        }

        potions.replace(potionType, potionCount - 1);
        if(potionType == PotionType.HEALTH) 
        {
            health += healthPotionEffect;

            sendTextToMob("> You drink a health potion and recover health!"
                    + "\n> You now have " + health + " HP.");

        }
        else if(potionType == PotionType.STRENGTH)
        {
            strengthEffect += randomEngine.nextInt(1, 4);
            sendTextToMob("You drank a strength potion and your attack is now: " + getAttackDamage() + "!");                
        }
        sendTextToMob("> You have " + getNumberOfPotions(potionType) + " " + potionName + " potions left.\n)");
    }


    public int getAttackDamage() {
        return super.getAttackDamage() * strengthEffect;
    }

    public void wonFight(Mob victim) {
        // Expire spells that only last a single combat
        strengthEffect = 1;

        sendTextToMob("-------------------------------------------------");
        sendTextToMob(" # " + victim.name + " was defeated! #");
        sendTextToMob(" # You have " + health + " HP left. #");     

        // Loot the victim
        if(victim.goldAmount > 0) {
            goldAmount += victim.goldAmount;
            sendTextToMob(" # The " + victim.name + " dropped " + victim.goldAmount + " gold! #");
            sendTextToMob(" # You now have " + goldAmount + " gold. #");
        }

        for(Item item : victim.items) {
            sendTextToMob("\n!!!# The " + victim.name + " has dropped a " + item.getName() + "!");
            equipItem(item);
        }
        for(Map.Entry<PotionType, Integer> entry : victim.potions.entrySet()) {
            addToInventory(entry.getKey(), entry.getValue());

            sendTextToMob(" # The " + victim.name + " dropped a " + entry.getKey().getName() + " potion! # ");
            sendTextToMob(" # You have " + getNumberOfPotions(entry.getKey()) + " " + entry.getKey().getName() + " potion(s). # ");
        }
    }


    @Override
    public void sendTextToMob(String text) {
        System.out.println(text);
    }

    @Override
    public String getInputFromMob() {
        return scanner.nextLine(); 
    }

    public boolean sacrificeIllbane() {
        if(illBaneCount >= 4) 
        {
            illBaneCount -= 4;
            return true;
        }
        sendTextToMob("You do not have enough illbane to make a worthy sacrifice!");            
        return false;
    }

    private void addToInventory(Item item, int count) {
        if(item.isIllBane()) {
            illBaneCount += count;
        } else if(item.isPotion()){
            addToInventory(item.getPotionType(), count);
        }
    } 
    
	public boolean isIllBaneAct() {
		return illBaneAct;
	}

	public void setIllBaneAct(boolean illBaneAct) {
		this.illBaneAct = illBaneAct;
	}
}
