package com.testm.fi8.domain;

import com.testm.fi8.enums.ItemType;
import com.testm.fi8.enums.PotionType;

public class Item {
    private String name;
    private int value;
    private int attackDamage;
    private int attackMultiplier;
    private int acAdjust;
    private PotionType potionType;
    private ItemType itemType;

    public Item(String name, int value, int attackDamage, int acAdjust, int attackMultiplier) {
        this(name, value, attackDamage, acAdjust, ItemType.EQUIPMENT, PotionType.NONE, attackMultiplier);
    }    

    public Item(String name, int value, ItemType itemType) {
        this(name,value,0,0, itemType, PotionType.NONE,0);
    }

    public Item(String name, int value, PotionType potionType) {
        this(name,value,0,0, ItemType.POTION, potionType,0);
    }

    private Item(String name, int value, int attackDamage, int acAdjust, ItemType itemType, PotionType potionType, int attackMultiplier) {
        this.name = name;
        this.value = value;
        this.attackDamage = attackDamage;
        this.acAdjust = acAdjust;
        this.itemType = itemType;
        this.potionType = potionType;
        this.attackMultiplier = attackMultiplier;
    }   

    public boolean isIllBane() {
        return itemType == ItemType.ILLBANE;
    }
    public boolean isPotion() {
        return itemType == ItemType.POTION;
    }
    public boolean isEquipment() {
        return itemType == ItemType.EQUIPMENT;
    }

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}

	public int getAttackDamage() {
		return attackDamage;
	}

	public int getAttackMultiplier() {
		return attackMultiplier;
	}

	public int getAcAdjust() {
		return acAdjust;
	}

	public PotionType getPotionType() {
		return potionType;
	}

	public ItemType getItemType() {
		return itemType;
	}
}
