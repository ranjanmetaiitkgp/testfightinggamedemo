package com.testm.fi8.factory;

import java.util.ArrayList;
import java.util.Random;

import com.testm.fi8.constant.NPCConstants;
import com.testm.fi8.domain.NPC;
import com.testm.fi8.util.RandomEngine;

public class NPCFactory {
    public NPCFactory(RandomEngine randomEngine){
        this.randomEngine = randomEngine;

        mobTemplates.add(new MobTemplate(NPCConstants.S, 50, 100, 25, -1, -1));
        mobTemplates.add(new MobTemplate(NPCConstants.M, 70, 150, 30, 55, 55));
        mobTemplates.add(new MobTemplate(NPCConstants.L, 90, 200, 40, 60, 60));
        mobTemplates.add(new MobTemplate(NPCConstants.XL, 150, 250, 50, 75, 75));

        bossTemplates.add(new MobTemplate(NPCConstants.XXL, 2500,1, 75, 50, 50, 20, 100, 200, 25));      
    }

    public NPC spawnEnemy(Random rand) {
        MobTemplate template = mobTemplates.get(rand.nextInt(mobTemplates.size()));
        return new NPC(template.name, template.baseHealth + rand.nextInt(template.healthAdjust), template.attackDamage, template.potionDropChance, template.goldDropChance, rand.nextInt(500) + 1, template.armorClass, template.specialAttackThreshold, template.specialAttack, template.specialDropChance, randomEngine);
    }

    public NPC spawnBoss(Random rand) {
        MobTemplate template = bossTemplates.get(rand.nextInt(bossTemplates.size()));
        return new NPC(template.name, template.baseHealth + rand.nextInt(template.healthAdjust), template.attackDamage, template.potionDropChance, template.goldDropChance, rand.nextInt(500) + 1000, template.armorClass, template.specialAttackThreshold, template.specialAttack, template.specialDropChance, randomEngine);
    }


    private class MobTemplate {
        public final String name;
        public int baseHealth;
        public int healthAdjust;
        public int attackDamage;
        public int potionDropChance;
        public int goldDropChance;
        public int armorClass;
        public int specialAttackThreshold;
        public int specialAttack;
        public int specialDropChance;       

        MobTemplate(String name, int baseHealth, int healthAdjust, int attackDamage, int potionDropChance, int goldDropChance) {
            this(name,baseHealth,healthAdjust,attackDamage,potionDropChance,goldDropChance,0,0,0,-1);
        }

        MobTemplate(String name, int baseHealth, int healthAdjust, int attackDamage, int potionDropChance, int goldDropChance, int armorClass, int specialAttackThreshold, int specialAttack, int specialDropChance) {
            this.name = name;
            this.baseHealth = baseHealth;
            this.healthAdjust = healthAdjust;
            this.attackDamage = attackDamage;
            this.potionDropChance = potionDropChance;
            this.goldDropChance = goldDropChance;
            this.armorClass = armorClass;
            this.specialAttackThreshold = specialAttackThreshold;
            this.specialAttack = specialAttack;
            this.specialDropChance = specialDropChance;
        }
    }

    private ArrayList<MobTemplate> mobTemplates = new ArrayList<MobTemplate>();
    private ArrayList<MobTemplate> bossTemplates = new ArrayList<MobTemplate>();
    private RandomEngine randomEngine;

}
