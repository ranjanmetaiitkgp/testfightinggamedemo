package com.testm.fi8.validator;

import java.util.Scanner;

import com.testm.fi8.domain.Player;

public class UserSelectionValidator {
	
	public static String getUserSelection(Player player, String prompt, String[] validSelections) {
        String input = "";
        do {
            if(!input.equals("")) {
                player.sendTextToMob("Sorry, I don't understand.");
            }
            player.sendTextToMob(prompt);

            input = player.getInputFromMob();
        }
        while(!isValidInput(input, validSelections));
        return input;
    }
	
	public static boolean isValidInput(String input, String[] validSelections) {
        for(String valid : validSelections) {
            if(input.equals(valid)) 
            	return true;
        }
        return false;
    }
	
	public static String validateUserName(String playerName, Scanner console) {
		while(playerName == null || playerName.equals("")) {
			System.out.println("Please provide a valid name");
			playerName = console.nextLine();
		}
		return playerName;
	}

}
