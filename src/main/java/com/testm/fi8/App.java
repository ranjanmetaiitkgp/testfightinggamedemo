package com.testm.fi8;

import com.testm.fi8.exception.GameInterruptedException;

public class App 
{
	public static void main(String[] args) {
		try {
	 		AppEngine.GAME.launch();
		} catch (GameInterruptedException e) {
			System.err.println(e.getMessage());
		}
	}
}
