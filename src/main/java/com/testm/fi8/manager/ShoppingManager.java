package com.testm.fi8.manager;

import java.util.Scanner;

import com.testm.fi8.domain.Item;
import com.testm.fi8.domain.Player;
import com.testm.fi8.domain.Shop;
import com.testm.fi8.enums.PotionType;
import com.testm.fi8.validator.UserSelectionValidator;

public class ShoppingManager {

	public void goShopping(Player player, Shop shop, Scanner console) {
		player.sendTextToMob("\nWelcome to the sshhop outssider...");
		player.sendTextToMob("What would you like to buy?");

		Item randomItem = shop.getRandomItemForSale();

		if (getYesNoResponse(player, String.format("\nWould you like to buy a %s ?\nThat will be %d gold.\n", randomItem.getName(), randomItem.getValue())))
		{
			if(player.canAfford(randomItem.getValue())) {
				shop.purchase(randomItem);
				player.purchasedItem(randomItem);
				player.sendTextToMob("Thank you for your purchase.");
				if(randomItem.isIllBane()) {
					player.sendTextToMob("See what you can do with a couple more of those...");                         
				}
			} else {
				player.sendTextToMob("You don't have enough money!");
			}
			return;
		}
		else
		{
			if(getYesNoResponse(player, "\nWould you like to buy some potions at least?"))
			{
				String input = UserSelectionValidator.getUserSelection(player, "\nHealth Potions or Strength Potions?\n" +
						"1. Health Potions: 100 gold\n" + 
						"2. Strength Potions: 500 gold\n" + 
						"3. Nevermind!", new String[]{"1","2","3"});

				if (input.equals("1") || input.equals("2"))
				{
					player.sendTextToMob("How many would you like to buy?");
					int inputNumH = Integer.parseInt(player.getInputFromMob());

					Item potion = shop.getPotion(input.equals("1") ? PotionType.HEALTH : PotionType.STRENGTH);

					if(player.canAfford(inputNumH * potion.getValue())) 
					{                       
						player.purchasedPotions(potion, inputNumH);

						player.sendTextToMob("Here you are: " + inputNumH + " " + potion.getValue() + ".");
					} 
					else 
					{
						player.sendTextToMob("You don't have enough money!");
					}                    
					return;
				}
				else
				{
					player.sendTextToMob("Stop wasting my time!");
					return;
				}
			} 
			else 
			{
				player.sendTextToMob("\nAlrighty.");
			}
		}
		return;     
	} 

	public boolean getYesNoResponse(Player player, String prompt) {
		return UserSelectionValidator.getUserSelection(player, String.format("%s\n1. Yes\n2. No", prompt), new String[]{"1","2"}).equals("1");
	}

}
