package com.testm.fi8.constant;

public class NPCConstants {
	
	public static final String S = "Erick Rowan";
	public static final String M = "Robin Hood";
	public static final String L = "Captain America";
	public static final String XL = "AquaMan";
	public static final String XXL = "HULK the Warrior";

}
