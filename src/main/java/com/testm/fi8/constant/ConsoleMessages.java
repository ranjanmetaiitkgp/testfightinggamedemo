package com.testm.fi8.constant;

public class ConsoleMessages {
	
	public static final String WELCOME_MESSAGE = "Welcome to ZEST Fighting Zone!!!";
	public static final String CREATE_PLAYER = "Create a Character ... Enter your character name:";
	public static final String FIGHTING_ZONE_ENTRY_PLAYER = "Player Entering the ZEST Fighting Zone: ";
	public static final String FIGHTING_ZONE_ENTRY_NPC = "Your Opponent Entering the ZEST Fighting Zone: ";
	
	public static final String HP_STATUS_PLAYER = "Your current HP status: ";
	public static final String HP_STATUS_NPC = "(Opponent) current HP status: ";
	
	public static final String DAMAGE_STATUS_PLAYER = "> You have taken too much damage, you are too weak to go on!\nYou crawl out of the game to live and fight another day.";
	public static final String Q_PERFORM_FIGHT = "What would you like to do?\n";
	public static final String Q_GAME_OPTION = "What to do now?\n";
	
	public static final String STRIKE_DAMAGE_NPC ="> You strike the opponent: ";
	public static final String STRIKE_DAMAGE_PLAYER ="(opponent) strike you ";
	public static final String DAMAGE = "The damage dealt: ";
			
}
