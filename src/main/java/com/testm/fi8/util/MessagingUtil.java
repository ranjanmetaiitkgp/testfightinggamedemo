package com.testm.fi8.util;

import com.testm.fi8.constant.ConsoleMessages;
import com.testm.fi8.enums.FightActionType;
import com.testm.fi8.enums.GameOptionType;

public class MessagingUtil {

	public static String getPerformFightMessage() {
		String msg = ConsoleMessages.Q_PERFORM_FIGHT;
		StringBuilder msgBuilder = new StringBuilder(msg);
		for (FightActionType action : FightActionType.values()) {
			msgBuilder.append(action.getId()).append(". ").append(action.getValue()).append("\n");
	    }
		return msgBuilder.toString();
	}
	
	public static String getGameOptionMessage() {
		String msg = ConsoleMessages.Q_GAME_OPTION;
		StringBuilder msgBuilder = new StringBuilder(msg);
		for (GameOptionType action : GameOptionType.values()) {
			msgBuilder.append(action.getId()).append(". ").append(action.getValue()).append("\n");
	    }
		return msgBuilder.toString();
	}
}
