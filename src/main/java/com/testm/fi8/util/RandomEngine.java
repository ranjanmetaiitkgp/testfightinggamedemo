package com.testm.fi8.util;

public interface RandomEngine {
    public int nextInt(int min, int max);
}
