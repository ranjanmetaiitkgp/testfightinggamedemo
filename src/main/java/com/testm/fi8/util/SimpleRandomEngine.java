package com.testm.fi8.util;

import java.util.Random;

public class SimpleRandomEngine implements RandomEngine {
    private Random rand;

    public SimpleRandomEngine(Random rand) {
        this.rand = rand;
    }

    public int nextInt(int min, int max) {
        return rand.nextInt(max-min) + min; 
    }
}
