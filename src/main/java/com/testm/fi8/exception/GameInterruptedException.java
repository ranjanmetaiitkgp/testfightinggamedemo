package com.testm.fi8.exception;

public class GameInterruptedException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7517824846052243667L;

	public GameInterruptedException(String message) {
		super(message);
	}
}
