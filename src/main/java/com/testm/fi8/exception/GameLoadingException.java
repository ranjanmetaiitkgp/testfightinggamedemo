package com.testm.fi8.exception;

public class GameLoadingException extends RuntimeException{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -94903271373680556L;

	public GameLoadingException(String message) {
		super(message);
	}

}
