package com.testm.fi8.action;

import com.testm.fi8.domain.Player;
import com.testm.fi8.enums.PotionType;

public class DrinkHealthPotionAction implements MenuOption{

	private Player player;
	
	public DrinkHealthPotionAction(Player player) {
		this.player = player;
	}
	
	@Override
	public void execute() {
		player.drinkPotion(PotionType.HEALTH);
	}

}
