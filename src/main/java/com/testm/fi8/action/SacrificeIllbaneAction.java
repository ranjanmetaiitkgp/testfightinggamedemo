package com.testm.fi8.action;

import java.util.Random;

import com.testm.fi8.domain.NPC;
import com.testm.fi8.domain.Player;
import com.testm.fi8.factory.NPCFactory;

public class SacrificeIllbaneAction implements MenuOption{

	private Player player;
	private NPCFactory npcFactory;
	private NPC enemy;
	private Random rand;
	
	public SacrificeIllbaneAction(Player player, NPC enemy, NPCFactory npcFactory, Random rand){
		this.player = player;
		this.npcFactory = npcFactory;
		this.enemy = enemy;
		this.rand = rand;
	}
	
	@Override
	public void execute() {
		player.sendTextToMob("-------------------------------------------------");
        player.sendTextToMob("# A great beast stirs! #");
        enemy = npcFactory.spawnBoss(rand);
        player.sendTextToMob("# " + enemy.getName() + " appears! #\n");
	}

}
