package com.testm.fi8.action;

import com.testm.fi8.domain.Player;

public class ContinueFightAction implements MenuOption{
	
	private Player player;
	
	public ContinueFightAction(Player player) {
		this.player = player;
	}

	@Override
	public void execute() {
		player.sendTextToMob("You continue on your trek through the dungeon.");
	}

}
