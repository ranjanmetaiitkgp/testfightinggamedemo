package com.testm.fi8.action;

import java.util.Scanner;

import com.testm.fi8.domain.Player;
import com.testm.fi8.domain.Shop;
import com.testm.fi8.manager.ShoppingManager;

public class VisitShopAction implements MenuOption{
	
	private Player player;
	private Shop shop;
	private Scanner console;
	private ShoppingManager shoppingManager = new ShoppingManager();
	
	public VisitShopAction(Player player, Shop shop, Scanner console) {
		this.player = player;
		this.shop = shop;
		this.console = console;
	}

	@Override
	public void execute() {
		shoppingManager.goShopping(player,shop,console);
	}

}
