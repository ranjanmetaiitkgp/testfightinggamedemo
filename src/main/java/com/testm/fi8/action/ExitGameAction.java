package com.testm.fi8.action;

import com.testm.fi8.domain.Player;

public class ExitGameAction implements MenuOption{

	private Player player;
	
	public ExitGameAction(Player player) {
		this.player = player;
	}
	
	@Override
	public void execute() {
		player.sendTextToMob("You exit the Game.");
	}

}
