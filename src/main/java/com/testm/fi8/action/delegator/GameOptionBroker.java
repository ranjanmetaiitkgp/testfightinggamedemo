package com.testm.fi8.action.delegator;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.testm.fi8.action.ContinueFightAction;
import com.testm.fi8.action.ExitGameAction;
import com.testm.fi8.action.MenuOption;
import com.testm.fi8.action.SacrificeIllbaneAction;
import com.testm.fi8.action.VisitShopAction;
import com.testm.fi8.domain.NPC;
import com.testm.fi8.domain.Player;
import com.testm.fi8.domain.Shop;
import com.testm.fi8.enums.GameOptionType;
import com.testm.fi8.factory.NPCFactory;

public class GameOptionBroker {
	
private BlockingQueue<MenuOption> playerCommand = new ArrayBlockingQueue<>(1);
	
	public void takeOrder(GameOptionType option, NPC enemy, Player player, Shop shop, Scanner console, NPCFactory npcFactory, Random rand) throws InterruptedException {
		prepareOrder(option,enemy,player,shop, console,npcFactory,rand);
	}
	
	private void prepareOrder(GameOptionType option, NPC enemy, Player player, Shop shop, Scanner console, NPCFactory npcFactory, Random rand) throws InterruptedException {
		if(option.equals(GameOptionType.CONTINUE_FIGHT)) {
			playerCommand.put(new ContinueFightAction(player));
		}else if(option.equals(GameOptionType.EXIT_GAME)) {
			playerCommand.put(new ExitGameAction(player));
		}else if(option.equals(GameOptionType.VISIT_SHOP)) {
			playerCommand.put(new VisitShopAction(player,shop,console));
		}else if(option.equals(GameOptionType.SACRIFICE_ILLBANE)) {
			playerCommand.put(new SacrificeIllbaneAction(player,enemy,npcFactory,rand));
		}
		placeOrder();
	}
	
	private void placeOrder() throws InterruptedException {
		playerCommand.take().execute();
	}

}
