package com.testm.fi8.action.delegator;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.testm.fi8.action.AttackAction;
import com.testm.fi8.action.DrinkHealthPotionAction;
import com.testm.fi8.action.DrinkStrengthPotionAction;
import com.testm.fi8.action.MenuOption;
import com.testm.fi8.action.RunAction;
import com.testm.fi8.domain.NPC;
import com.testm.fi8.domain.Player;
import com.testm.fi8.enums.FightActionType;

public class FightActionBroker {
	
	private BlockingQueue<MenuOption> playerCommand = new ArrayBlockingQueue<>(1);
	
	public void takeOrder(FightActionType option, NPC enemy, Player player) throws InterruptedException {
		prepareOrder(option, enemy, player);
	}
	
	private void prepareOrder(FightActionType option, NPC enemy, Player player) throws InterruptedException {
		if(option.equals(FightActionType.ATTACK)) {
			playerCommand.put(new AttackAction(enemy,player));
		}else if(option.equals(FightActionType.DRINK_HEALTH_POTION)) {
			playerCommand.put(new DrinkHealthPotionAction(player));
		}else if(option.equals(FightActionType.RUN)) {
			playerCommand.put(new RunAction(player,enemy));
		}else if(option.equals(FightActionType.DRINK_STRENGTH_POTION)) {
			playerCommand.put(new DrinkStrengthPotionAction(player));
		}
		placeOrder();
	}
	
	private void placeOrder() throws InterruptedException {
		playerCommand.take().execute();
	}

}
