package com.testm.fi8.action.delegator;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.testm.fi8.action.MenuOption;

public class ShoppingActionBroker {
	
	private BlockingQueue<MenuOption> playerCommand = new ArrayBlockingQueue<>(1);
	
	public void takeOrder() {
		prepareOrder();
	}
	
	private void prepareOrder() {
		placeOrder();
	}
	
	private void placeOrder() {
		
	}

}
