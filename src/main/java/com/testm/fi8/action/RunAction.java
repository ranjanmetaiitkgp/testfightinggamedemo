package com.testm.fi8.action;

import com.testm.fi8.domain.NPC;
import com.testm.fi8.domain.Player;

public class RunAction implements MenuOption{
	
	private NPC enemy;
	private Player player;
	
	public RunAction(Player player, NPC enemy) {
		this.player = player;
		this.enemy = enemy;
	}

	@Override
	public void execute() {
		 player.sendTextToMob("You run away from the " + enemy.getName() + "!");
	}

}
