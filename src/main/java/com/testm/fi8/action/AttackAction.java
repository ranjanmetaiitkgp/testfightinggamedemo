package com.testm.fi8.action;

import com.testm.fi8.domain.NPC;
import com.testm.fi8.domain.Player;

public class AttackAction implements MenuOption{
	
	private NPC enemy;
	private Player player;
	
	public AttackAction(NPC enemy, Player player) {
		this.enemy = enemy;
		this.player = player;
	}

	@Override
	public void execute() {
		player.performHit(enemy);
        enemy.performHit(player); 
	}

}
