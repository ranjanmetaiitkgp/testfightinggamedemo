package com.testm.fi8.action;

import com.testm.fi8.domain.Player;
import com.testm.fi8.enums.PotionType;

public class DrinkStrengthPotionAction implements MenuOption{
	
	private Player player;
	
	public DrinkStrengthPotionAction(Player player) {
		this.player = player;
	}

	@Override
	public void execute() {
		player.drinkPotion(PotionType.STRENGTH);
	}

}
