package com.testm.fi8.enums;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum GameOptionType{
	
	CONTINUE_FIGHT(1, "Continue fighting"),EXIT_GAME(2,"Exit Game"), VISIT_SHOP(3, "Visit the shop"), SACRIFICE_ILLBANE(4,"Sacrifice Illbane..."); 
	//,PAUSE(5,"Pause Game"), RESUME(6, "Resume Game from previous state");
	
	private int id;
	private String value;
	private static final Map<Integer, GameOptionType> lookUpMap = Collections.unmodifiableMap(initializeMapping());
	
	private GameOptionType(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public String getValue() {
		return value;
	}

	private static Map<Integer, GameOptionType> initializeMapping() {
		Map<Integer, GameOptionType> lookUpMap = new HashMap<Integer, GameOptionType>();
	    for (GameOptionType action : GameOptionType.values()) {
	    	lookUpMap.put(action.id, action);
	    }
	    return lookUpMap;
	}

	public static GameOptionType getGameOption(int key) {
		return lookUpMap.get(key);
	}
}
