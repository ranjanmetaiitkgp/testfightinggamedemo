package com.testm.fi8.enums;

public enum EquipmentType {
	
	SILVER_SWORD("Silver Sword"), STEEL_SWORD("Steel Sword"), IRON_HELMET("Iron Helmet"), IRON_CHESTPLATE("Iron Chestplate"), 
	IRON_BOOTS("Iron Boots"), IRON_GAUNTLETS("Iron Gauntlets"), STEEL_HELMET("Steel Helmet"), STEEL_CHESTPLATE("Steel Chestplate"), 
	STEEL_BOOTS("Steel Boots"), STEEL_GAUNTLETS("Steel Gauntlets");
	
	private String equipmentName;
	
	private EquipmentType(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

}
