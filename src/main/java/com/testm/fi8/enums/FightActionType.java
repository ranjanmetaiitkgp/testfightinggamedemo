package com.testm.fi8.enums;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum FightActionType {
	
	ATTACK(1, "Attack"), DRINK_HEALTH_POTION(2, "Drink health potion"), RUN(3, "Run!"), DRINK_STRENGTH_POTION(4,"Drink strength potion");
	
	private int id;
	private String value;
	private static final Map<Integer, FightActionType> lookUpMap = Collections.unmodifiableMap(initializeMapping());
	
	private FightActionType(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public String getValue() {
		return value;
	}
	
	private static Map<Integer, FightActionType> initializeMapping() {
		Map<Integer, FightActionType> lookUpMap = new HashMap<Integer, FightActionType>();
	    for (FightActionType action : FightActionType.values()) {
	    	lookUpMap.put(action.id, action);
	    }
	    return lookUpMap;
	}

	public static FightActionType getFightAction(int key) {
		return lookUpMap.get(key);
	}

}
