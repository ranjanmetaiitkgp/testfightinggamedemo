package com.testm.fi8.enums;

public enum PotionType {
    HEALTH (0, "Health"),
    STRENGTH(1, "Strength"),
    NONE(2, "None");

   private PotionType(int id, String name) {
        this.id = id;
        this.name= name;
    }

    private int id;
    private String name;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
