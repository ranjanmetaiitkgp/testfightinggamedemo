package com.testm.fi8;

import java.util.Random;
import java.util.Scanner;

import com.testm.fi8.action.delegator.FightActionBroker;
import com.testm.fi8.action.delegator.GameOptionBroker;
import com.testm.fi8.constant.ConsoleMessages;
import com.testm.fi8.domain.NPC;
import com.testm.fi8.domain.Player;
import com.testm.fi8.domain.Shop;
import com.testm.fi8.enums.FightActionType;
import com.testm.fi8.enums.GameOptionType;
import com.testm.fi8.exception.GameInterruptedException;
import com.testm.fi8.exception.GameLoadingException;
import com.testm.fi8.factory.NPCFactory;
import com.testm.fi8.util.MessagingUtil;
import com.testm.fi8.util.RandomEngine;
import com.testm.fi8.util.SimpleRandomEngine;
import com.testm.fi8.validator.UserSelectionValidator;

public enum AppEngine {
	
	GAME;

	private Scanner console;
	private RandomEngine randomEngine;
	private Shop shop;
	private Random rand;
	private NPCFactory npcFactory;
	private Player player;

	private void loadEngine() throws GameLoadingException{
		try {
			rand = new Random();
			randomEngine = new SimpleRandomEngine(rand);

			shop = new Shop(randomEngine);
			npcFactory = new NPCFactory(randomEngine);
			
			System.out.println(ConsoleMessages.WELCOME_MESSAGE);
			System.out.println(ConsoleMessages.CREATE_PLAYER);
			
			console = new Scanner(System.in);
			String playerName = console.nextLine();
			playerName = UserSelectionValidator.validateUserName(playerName, console);
			player = new Player(playerName, randomEngine, console);
			player.sendTextToMob(ConsoleMessages.FIGHTING_ZONE_ENTRY_PLAYER + player.getName());
		}catch (RuntimeException e) {
			throw new GameLoadingException("Exception in loading Game. Please try again!!!");
		}
	}

	public void launch() throws GameInterruptedException 
	{
		try {
			loadEngine();

			GameOptionBroker gob = new GameOptionBroker();

			while(player.isAlive()) 
			{
				player.sendTextToMob("-------------------------------------------------");
				NPC enemy = npcFactory.spawnEnemy(rand);
				player.sendTextToMob(ConsoleMessages.FIGHTING_ZONE_ENTRY_NPC + enemy.getName());

				if(!performFight(enemy, player)) {
					continue;
				}

				String input = UserSelectionValidator.getUserSelection(player, 
						"\n-------------------------------------------------\n" + MessagingUtil.getGameOptionMessage(), new String[]{"1","2","3","4"});

				try {
					GameOptionType gameOptionType = GameOptionType.getGameOption(Integer.parseInt(input));
					gob.takeOrder(gameOptionType, enemy, player, shop, console,npcFactory,rand);
					if(GameOptionType.EXIT_GAME.equals(gameOptionType)) {
						break;
					}else if(player.isIllBaneAct()) {
						if(!performFight(enemy, player)) {
							continue;
						}
					}

				} catch (NumberFormatException | InterruptedException e1) {
					throw new GameInterruptedException("OOPS!!! Something went wrong in game option command.");
				} 
			}
		}catch(GameLoadingException e) {
			System.err.println(e.getMessage());
		}
		
	}

	public boolean performFight(NPC enemy, Player player) throws GameInterruptedException {
		FightActionBroker fab = new FightActionBroker();
		while(enemy.isAlive() && player.isAlive())
		{
			// Enemy introduction and presentation of options.
			player.sendTextToMob(ConsoleMessages.HP_STATUS_PLAYER + player.getHealth());
			player.sendTextToMob(enemy.getName() + ConsoleMessages.HP_STATUS_NPC + enemy.getHealth());

			String input = UserSelectionValidator.getUserSelection(player, MessagingUtil.getPerformFightMessage(), new String[] {"1","2","3","4"});

			try {
				FightActionType fightActiontype = FightActionType.getFightAction(Integer.parseInt(input));
				fab.takeOrder(fightActiontype, enemy, player);
				if(FightActionType.RUN.equals(fightActiontype)) {
					return false;
				}
			} catch (NumberFormatException | InterruptedException e1) {
				throw new GameInterruptedException("OOPS!!! Something went wrong in fight action command.");
			}

		}

		if(!player.isAlive())
		{
			player.sendTextToMob(ConsoleMessages.DAMAGE_STATUS_PLAYER);
			return false;
		}   
		player.wonFight(enemy);
		return true;
	}
}
